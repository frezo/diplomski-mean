# Update sustava prije početka rada
sudo apt-get update

# Instalacija Git-a (nije obavezno)
sudo apt-get install -y git

# Instalacija NodeJS-a
sudo apt-get install -y curl
curl -sL https://deb.nodesource.com/setup_7.x | sudo -E bash -
sudo apt-get install -y nodejs
# npm init
# npm install express body-parser ejs mongoose --save

sudo npm install -g nodemon # Alternativa za 'forever'

#Instalirati 'forever' koji će nadzirati aplikaciju
# sudo npm install -g forever
# forever --watch --watchDirectory /home/vagrant/video-app-diplomski/MEAN_video_app
# forever start server.js

# Instalacija MongoDB-a i pokretanje MongoDB servera
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 0C49F3730359A14518585931BC711F9BA15703C6
echo "deb [ arch=amd64 ] http://repo.mongodb.org/apt/ubuntu precise/mongodb-org/3.4 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.4.list
sudo apt-get update
sudo apt-get install -y mongodb-org
sudo service mongod start

# Update sustava prije pokretanja NodeJS servera (nije obavezno)
sudo apt-get update

# Pozicionirat se u mapu projekta i pokrenuti NodeJS server
# cd diplomski-mean
# forever --watch --watchDirectory /home/vagrant/video-app-diplomski/MEAN_video_app
# forever start server.js
# forever -o out.log -e err.log server.js

# Paljenje NodeJS servera
# nodemon 