var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var mongoose = require('mongoose');

var Camera = require('../models/camera');
var Image = require('../models/image');

app.get('/', function(req, res) {
    res.sendFile(__dirname + '/index.html');
});

mongoose.connect('localhost:27017/node-angular');

var imgIndex = 0;
var lastImg;
var conn;

io.on('connection', function(socket) {

    socket.on('disconnect', function() {
        console.log('user disconnected');
        console.log('konekcija je: ', conn);
        clearInterval(conn);
        console.log('na kraju je conn: ', conn);
    });
    

    socket.on('save image', function() {
        console.log('on save image');
        postNewImages();
    }); 
    
});


function postNewImages() {
    conn = setInterval( function () {
        executeSavingImages();
    }, 1000);
}

function executeSavingImages() {
    Camera.find()
    .exec(function(err, cameras) {
        cameras.forEach(function(camera, i) {
            var imgDirIndex = (i % 5) + 1;
            var imgNumber = (imgIndex % 10) + 1;
            var imgName = imgNumber + ".jpg";
            var image = new Image({
                name: imgName,
                date: new Date(),
                imgUrl: "/images/camera" + imgDirIndex + "/" + imgName,
                camera: camera
            });
            image.save(function(err, result) {
                if (err) {
                    console.error(err);
                } else {
                    io.emit('lastImage', {image: result, cameraId: result.camera._id});
                }
            });
        }, this);
        imgIndex++;
    });
}

http.listen(3001, function() {
    console.log("Listening on 3001...");
});

module.exports = app;

