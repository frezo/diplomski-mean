var express = require('express');
var router = express.Router();
var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');

var User = require('../models/user');
var Camera = require('../models/camera');
var Layout = require('../models/layout');

router.post('/login', function(req, res, next) {
    User.findOne({username: req.body.username}, function(err, user) {
        if(err) {
            return res.status(500).json({
                title: 'Dogodila se greška pri logiranju!',
                error: err
            });
        }
        if (!user) {
            return res.status(500).json({
                title: 'Neuspjela prijava!',
                error: {message: 'Korisnik s tim imenom nije pronađen!'}
            });
        }
        if(!bcrypt.compareSync(req.body.password, user.password)) {
            return res.status(401).json({
                title: 'Neuspjela prijava!',
                error: {message: 'Lozinka nije ispravna!'}
            });
        }
        var token = jwt.sign({user: user}, 'diplomski-rad', {expiresIn: 57600});
        res.status(200).json({
            message: 'Uspješno logiranje!',
            token: token,
            user: user
        });
    })
});

router.post('/first-login', function(req, res, next) {
    var user = new User({
        username: req.body.username,
        password: bcrypt.hashSync(req.body.password, 10),
        firstName: req.body.firstName ? req.body.firstName : null,
        lastName: req.body.lastName ? req.body.lastName : null,
        email: req.body.email ? req.body.email : null,
        level: req.body.level
    });
    user.save(function(err, result) {
        if(err) {
            return res.status(500).json({
                title: 'Dogodila se greška pri kreiranju novog korisnika!',
                error: err
            });
        }
        res.status(201).json({
            message: 'Novi korisnik kreiran',
            obj: result
        });
    });
});

router.use('/', function (req, res, next) {
    jwt.verify(req.query.token, 'diplomski-rad', function (err, decoded) {
        if (err) {
            return res.status(401).json({
                title: 'Niste ovlašteni!',
                error: err
            });
        }
        next();
    });
});

router.get('/', function (req, res, next) {
    User.find( { qty: { $ne: 20 } })
        .exec(function(err, users) {
            if(err) {
                return res.status(500).json({
                    title: 'Dogodila se greška',
                    error: err
                });
            }  
            res.status(200).json({
                message: 'Sve je u redu',
                obj: users
            });
        });
});

router.get('/not/:id', function (req, res, next) {
    User.find( { _id: { $ne: req.params.id } })
        .exec(function(err, users) {
            if(err) {
                return res.status(500).json({
                    title: 'Dogodila se greška',
                    error: err
                });
            }  
            res.status(200).json({
                message: 'Sve je u redu',
                obj: users
            });
        });
});

router.get('/:id', function(req, res, next) {
    User.findById(req.params.id, function(err, user) {
        if(err) {
            return res.status(500).json({
                title: 'Dogodila se greška',
                error: err
            });
        }
        if(!user) {
            return res.status(500).json({
                title: 'Nije pronađen korisnik!',
                error: { message: 'Nepostojeći korisnik!'}
            });
        }
    })
    .exec(function(err, user) {
        if(err) {
            return res.status(500).json({
                title: 'Dogodila se greška',
                error: err
            });
        }  
        res.status(200).json({
            message: 'Sve je u redu',
            obj: user
        });
    });
});

router.post('/signup', function(req, res, next) {
    var user = new User({
        username: req.body.username,
        password: bcrypt.hashSync(req.body.password, 10),
        firstName: req.body.firstName ? req.body.firstName : null,
        lastName: req.body.lastName ? req.body.lastName : null,
        email: req.body.email ? req.body.email : null,
        level: req.body.level
    });
    user.save(function(err, result) {
        if(err) {
            return res.status(500).json({
                title: 'Dogodila se greška pri kreiranju novog korisnika!',
                error: err
            });
        }
        res.status(201).json({
            message: 'Novi korisnik kreiran',
            obj: result
        });
    });
});

router.patch('/:id', function (req, res, next) {
    User.findById(req.params.id, function (err, user) {
        if (err) {
            return res.status(500).json({
                title: 'Dogodila se greška',
                error: err
            });
        }
        if (!user) {
            return res.status(500).json({
                title: 'Korisnik nije pronađen!',
                error: {message: 'Nepostojeći korisnik!'}
            });
        }

        user.firstName = req.body.firstName;
        user.lastName = req.body.lastName;
        user.email = req.body.email;
        user.level = req.body.level;
        
        user.save(function(err, result) {
            if (err) {
                return res.status(500).json({
                    title: 'Dogodila se greška prilikom ažuriranja korisnika',
                    error: err
                });
            }
            res.status(200).json({
                message: 'Korisnik ažuriran!',
                obj: result
            });
        });
    });
});

router.patch('/password/:id', function (req, res, next) {
    User.findById(req.params.id, function (err, user) {
        if (err) {
            return res.status(500).json({
                title: 'Dogodila se greška',
                error: err
            });
        }
        if (!user) {
            return res.status(500).json({
                title: 'Korisnik nije pronađen!',
                error: {message: 'Nepostojeći korisnik!'}
            });
        }
        if(!bcrypt.compareSync(req.body.oldPassword, user.password)) {
            return res.status(401).json({
                title: 'Neispravna lozinka!',
                error: {message: 'Unesena lozinka nije ispravna!'}
            });
        }
        user.password = bcrypt.hashSync(req.body.newPassword, 10);
        
        user.save(function(err, result) {
            if (err) {
                return res.status(500).json({
                    title: 'Dogodila se greška prilikom ažuriranja korisnika',
                    error: err
                });
            }
            res.status(200).json({
                message: 'Lozinka korisnika uspješno ažurirana!',
                obj: result
            });
        });
    });
});

router.delete('/:id', function(req, res, next) {
    User.findById(req.params.id, function(err, user) {
        if (err) {
            return res.status(500).json({
                title: 'Dogodila se greška!',
                error: err
            });
        }
        if(!user) {
            return res.status(500).json({
                title: 'Nije pronađen korisnik!',
                error: { message: 'Nepostojeći korisnik!'}
            });
        }

        Layout.findOne({ user: req.params.id }, function (err, layout) {
            if(err) {
                return res.status(500).json({
                    title: 'Dogodila se greška pri dohvaćanju layout-a od korisnika!',
                    error: err
                });
            }
            if (!layout) {
                return res.status(500).json({
                    title: 'Traženi layout nije pronađen!',
                    error: {message: 'Traženi layout nije pronađen!'}
                });
            }
            layout.remove(function(err, deletedLayout) {
                if (err) {
                    return res.status(500).json({
                        title: 'Dogodila se greška prilikom brisanja layout-a korisnika!',
                        error: err
                    });
                }
            });
        });

        user.remove(function(err, deletedUser) {
            if(err) {
                return res.status(500).json({
                    title: 'Dogodila se greška prilikom brisanja korisnika!',
                    error: err
                });
            }

            res.status(200).json({
                camera: 'Korisnik je obrisan!',
                obj: deletedUser
            });
        });
    });
});

module.exports = router;