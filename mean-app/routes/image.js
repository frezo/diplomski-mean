var express = require('express');
var router = express.Router();

var Camera = require('../models/camera');
var Image = require('../models/image');

let interval = null;

router.get('/:id', function(req, res, next) {
    Image.find({camera: req.params.id}, function(err, images) {
        if(err) {
            return res.status(500).json({
                title: 'Dogodila se greška',
                error: err
            });
        }
    })
    .exec(function(err, images) {
        if(err) {
            return res.status(500).json({
                title: 'Dogodila se greška',
                error: err
            });
        }  
        res.status(200).json({
            message: 'Sve je u redu',
            obj: images
        })
    });
});

router.post('/', function(req, res, next) {
    var image = new Image({
        name: req.body.name,
        date: req.body.date,
        imgUrl: req.body.imgUrl,
        camera: req.body.camera
    });
    image.save(function(err, result) {
        if(err) {
            return res.status(500).json({
                title: 'Dogodila se greška pri kreiranju nove slike!',
                error: err
            });
        }
        res.status(201).json({
            message: 'Novi slika kreirana',
            obj: result
        });
    });
});

router.get('/:startDate/:endDate/:cameraId', function(req, res, next) {
    Image.find({ camera: req.params.cameraId })
    .where('date').gt(req.params.startDate).lt(req.params.endDate)
    .exec(function(err, images) {
        if(err) {
            return res.status(500).json({
                title: 'Dogodila se greška!',
                error: err
            });
        }
        res.status(201).json({
            message: 'Uspješno dohvaćene slike!',
            obj: images
        });
    });
});

function someFunc () {
    interval = setInterval( () => {
        console.log('helkasjl');
    }, 1000);
}

module.exports = { 
  router:router,
  someFunc:someFunc
}