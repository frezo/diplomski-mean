var express = require('express');
var router = express.Router();

var Camera = require('../models/camera');
var Layout = require('../models/layout');
var User = require('../models/user');

router.get('/', function (req, res, next) {
    Camera.find()
        .exec(function(err, cameras) {
            if(err) {
                return res.status(500).json({
                    title: 'Dogodila se greška',
                    error: err
                });
            }  
            res.status(200).json({
                message: 'Sve je u redu',
                obj: cameras
            })
        });
    // res.render('index');
});

router.get('/:id', function(req, res, next) {
    Camera.findById(req.params.id, function(err, camera) {
        if(err) {
            return res.status(500).json({
                title: 'Dogodila se greška',
                error: err
            });
        }
        if(!camera) {
            return res.status(500).json({
                title: 'Nije pronađena kamera!',
                error: { message: 'Nepostojeća kamera!'}
            });
        }
    })
    .exec(function(err, camera) {
        if(err) {
            return res.status(500).json({
                title: 'Dogodila se greška',
                error: err
            });
        }  
        res.status(200).json({
            message: 'Sve je u redu',
            obj: camera
        })
    });
})

router.get('/test/:id', function(req, res, next) {
    /*Layout.find({ 'availableCameras': { $in: [req.params.id] } }, function(err, layouts) {
        if(err) {
            return res.status(500).json({
                title: 'Dogodila se greška!',
                error: err
            });
        }
        if(!layouts) {
            return res.status(500).json({
                title: 'Nije pronađena kamera!',
                error: { message: 'Nepostojeća kamera!'}
            });
        }
    })
    .populate('user')
    .exec(function(err, result) {
        if(err) {
            return res.status(500).json({
                title: 'Dogodila se greška!',
                error: err
            });
        }
        cameraIDs = [];
        result.forEach(function(layout) {
            console.log(layout.user);
        }, this);
        res.status(201).json({
            message: 'Pronađeni layouti...',
            obj: result
        });
    });*/
    User.find({ level: 1 }, function (err, users) {
        if(err) {
            return res.status(500).json({
                title: 'Dogodila se greška pri dohvaćanju svih admina!',
                error: err
            });
        }
        res.status(201).json({
            message: 'Svi admini dohvaćeni!',
            obj: users
        });
    });
});

router.post('/', function(req, res, next) {
    var camera = new Camera({
        name: req.body.name,
        location: req.body.location,
        ip_address: req.body.ip_address,
        image_format: req.body.image_format ? req.body.image_format : null,
        camera_type: req.body.camera_type ? req.body.camera_type : null
    });

    camera.save(function(err, createdCamera) {
        if(err) {
            return res.status(500).json({
                title: 'Dogodila se greška pri kreiranju nove kamere!',
                error: err
            });
        }
        User.find({ level: 1 }, function (err, admins) {
            if(err) {
                return res.status(500).json({
                    title: 'Dogodila se greška pri dohvaćanju svih admina!',
                    error: err
                });
            }
            admins.forEach(function(admin) {
                Layout.findOne({ user: admin._id }, function (err, layout) {
                    if(err) {
                        return res.status(500).json({
                            title: 'Dogodila se greška pri dohvaćanju layout-a od admina!',
                            error: err
                        });
                    }
                    layout.availableCameras.push(createdCamera);
                    layout.save(function (err, result) {
                        if(err) {
                            return res.status(500).json({
                                title: 'Dogodila se greška pri update-u layout-a admina!',
                                error: err
                            });
                        }
                    });
                });
            }, this);
        });

        res.status(201).json({
            message: 'Uspješno dodana nova kamera i ažurirane kamere dostupne adminima!',
            obj: createdCamera
        });
    });
});


router.patch('/:id', function (req, res, next) {
    Camera.findById(req.params.id, function (err, camera) {
        if (err) {
            return res.status(500).json({
                title: 'Dogodila se greška',
                error: err
            });
        }
        if (!camera) {
            return res.status(500).json({
                title: 'Kamera nije pronađena!',
                error: {message: 'Kamera nije pronađena'}
            });
        }
        camera.name = req.body.name;
        camera.location = req.body.location;
        camera.ip_address = req.body.ip_address;
        camera.image_format = req.body.image_format;
        camera.camera_type = req.body.camera_type;
        
        camera.save(function(err, result) {
            if (err) {
                return res.status(500).json({
                    title: 'Dogodila se greška prilikom ažuriranja kamere',
                    error: err
                });
            }
            
            res.status(200).json({
                message: 'Kamera ažurirana',
                obj: result
            });
        });
    });
});


router.delete('/:id', function(req, res, next) {
    Camera.findById(req.params.id, function(err, camera) {
        if(err) {
            return res.status(500).json({
                title: 'Dogodila se greška',
                error: err
            });
        }
        if(!camera) {
            return res.status(500).json({
                title: 'Nije pronađena kamera!',
                error: { message: 'Nepostojeća kamera!'}
            });
        }

        User.find({}, function (err, users) {
            if(err) {
                return res.status(500).json({
                    title: 'Dogodila se greška pri dohvaćanju svih korisnika!',
                    error: err
                });
            }
            users.forEach(function(user) {
                Layout.findOne({ user: user._id }, function (err, layout) {
                    if(err) {
                        return res.status(500).json({
                            title: 'Dogodila se greška pri dohvaćanju layout-a od korisnika!',
                            error: err
                        });
                    }
                    
                    var isInAvailableCamArray = layout.availableCameras.some(function (availCam) {
                        return availCam.equals(camera._id);
                    });
                    if (isInAvailableCamArray) {
                        layout.availableCameras.pull(camera);
                    }

                    var isInCheckedCamArray = layout.checkedCameras.some(function (checkCam) {
                        return checkCam.equals(camera._id);
                    });
                    if (isInCheckedCamArray) {
                        layout.checkedCameras.pull(camera);
                    }

                    layout.save(function (err, result) {
                        if(err) {
                            return res.status(500).json({
                                title: 'Dogodila se greška pri update-u layout-a admina!',
                                error: err
                            });
                        }
                    });
                });
            }, this);
        });

        camera.remove(function(err, deletedCamera) {
            if(err) {
                return res.status(500).json({
                    title: 'Dogodila se greška prilikom brisanja kamere!',
                    error: err
                });
            }

            res.status(200).json({
                camera: 'Kamera je obrisana, te su uklonjene kamere s pretplaćenih korisnika!',
                obj: deletedCamera
            })
        });
    });
});

module.exports = router;