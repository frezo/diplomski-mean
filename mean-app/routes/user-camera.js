var express = require('express');
var router = express.Router();
var jwt = require('jsonwebtoken');

var User = require('../models/user');
var Camera = require('../models/camera');
var Layout = require('../models/layout');

router.get('/:id', function (req, res, next) {
    /*var decoded = jwt.decode(req.query.token);*/
    Layout.findOne({user: req.params.id}, function(err, layout) {
        if (err) {
            return res.status(500).json({
                title: 'Dogodila se greška',
                error: err
            });
        }
        if (!layout) {
            return res.status(500).json({
                title: 'Korisnik nije pronađen!',
                error: {message: 'Korisnik nije pronađen!'}
            });
        }
    })
    .populate('availableCameras checkedCameras')
    .exec(function(err, layout) {
        if(err) {
            return res.status(500).json({
                title: 'Dogodila se greška prilikom dohvaćanja mapiranih kamera za korisnika',
                error: err
            });
        }  
        res.status(200).json({
            message: 'Uspješno dohvaćene mapirane kamere za korisnika!',
            obj: layout
        })
    });
});

router.post('/init', function(req, res, next) {
    var layout = new Layout({
        user: req.body.user,
        availableCameras: req.body.cameras
    });
    layout.save(function(err, result) {
        if(err) {
            return res.status(500).json({
                title: 'Dogodila se greška pri mapiranju korisnika i kamera!',
                error: err
            });
        }
        res.status(201).json({
            message: 'Uspješno mapiranje korisnika i kamera!',
            obj: result
        });
    });
});

router.post('/', function(req, res, next) {
    var decoded = jwt.decode(req.query.token);
    User.findById(decoded.user._id, function(err, user) {
        if(err) {
            return res.status(500).json({
                title: 'Dogodila se greška!',
                error: err
            });
        }
        if(!user) {
            return res.status(500).json({
                title: 'Nije pronađen korisnik!',
                error: { message: 'Nepostojeći korisnik!'}
            });
        }
        Camera.findById(req.body.id, function(err, camera) {
            if(err) {
                return res.status(500).json({
                    title: 'Dogodila se greška!',
                    error: err
                });
            }
            var layout = new Layout({
                user: user
            });
            console.log(camera);
            layout.cameras.push(camera);
            console.log(layout);
            layout.save(function(err, result) {
                if(err) {
                    return res.status(500).json({
                        title: 'Dogodila se greška pri mapiranju korisnika i kamera!',
                        error: err
                    });
                }
                res.status(201).json({
                    message: 'Uspješno mapiranje korisnika i kamera!',
                    obj: result
                });
            });
        });
        
        
    });
});

router.patch('/', function (req, res, next) {
    var decoded = jwt.decode(req.query.token);
    Layout.findOne({user: decoded.user}, function(err, layout) {
        if (err) {
            return res.status(500).json({
                title: 'Dogodila se greška',
                error: err
            });
        }
        if (!layout) {
            return res.status(500).json({
                title: 'Traženi layout nije pronađen!',
                error: {message: 'Traženi layout nije pronađen!'}
            });
        }

        Camera.findById(req.body.cameraId, function(err, camera) {
            if (err) {
                return res.status(500).json({
                    title: 'Dogodila se greška',
                    error: err
                });
            }
            if (!camera) {
                return res.status(500).json({
                    title: 'Kamera nije pronađena!',
                    error: {message: 'Kamera nije pronađena!'}
                });
            }
            if (req.body.checked) {
                layout.checkedCameras.push(camera);
            } else {
                layout.checkedCameras.pull(camera);
            }
            console.log(req.body);
            console.log(layout);
            layout.save(function(err, result) {
                    if (err) {
                        return res.status(500).json({
                            title: 'Dogodila se greška prilikom ažuriranja mapiranih kamera za korisnika',
                            error: err
                        });
                    }
                    result.populate('checkedCameras', function(err, layout) {
                        if (err) {
                            return res.status(500).json({
                                title: 'Dogodila se greška prilikom ažuriranja mapiranih kamera za korisnika',
                                error: err
                            });
                        }
                        res.status(200).json({
                            message: 'Uspješno ažirarano mapiranje kamera za korisnika!',
                            obj: result
                        });
                    });
                });
        });
    });
});

router.patch('/:id', function (req, res, next) {
    Layout.findOne({user: req.params.id}, function(err, layout) {
        if (err) {
            return res.status(500).json({
                title: 'Dogodila se greška',
                error: err
            });
        }
        if (!layout) {
            return res.status(500).json({
                title: 'Traženi layout nije pronađen!',
                error: {message: 'Traženi layout nije pronađen!'}
            });
        }

        if (req.body.checked) {
            layout.availableCameras.push(req.body.camera);
        } else {
            layout.availableCameras.pull(req.body.camera);
        }
        layout.save(function(err, result) {
            if (err) {
                return res.status(500).json({
                    title: 'Dogodila se greška prilikom ažuriranja mapiranih kamera za korisnika',
                    error: err
                });
            }
            res.status(200).json({
                message: 'Uspješno ažirarano mapiranje kamera za korisnika!',
                obj: result
            });
        });
    });
});

module.exports = router;