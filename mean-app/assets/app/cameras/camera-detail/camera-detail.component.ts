import { UserService } from './../../users/user.service';
import { ImageService } from './../camera-archive/image.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Camera } from "../camera.model";
import { CameraService } from "../camera.service";

@Component({
    selector: 'app-camera-detail',
    templateUrl: './camera-detail.component.html'
})

export class CameraDetailComponent implements OnInit {
    camera: Camera;
    user;

    lastImage;
    connection;

    constructor(
        private route: ActivatedRoute,
        private cameraService: CameraService,
        private imageService: ImageService,
        private userService: UserService,
        private router: Router
    ) { 
        this.userService.loggedInUser$
            .subscribe(
                data => this.user = data,
                error => console.log(error)
            );
    }

    ngOnInit() {
        this.cameraService.getSingleCamera(this.route.snapshot.params['id'])
            .subscribe(
                (camera: Camera) => { 
                    this.camera = camera;
                    this.specFunc();
                }
            );
        
        this.route.params
            .subscribe(
                (params: Params) => {
                    this.cameraService.getSingleCamera(params['id'])
                    .subscribe(camera => {
                        this.camera = camera;
                        this.specFunc();
                    });
                }
            );

        this.cameraService.camerasCopy$
			.subscribe(
				(cameras: Camera[]) => {
                    for (var camera in cameras) {
                        if (cameras.hasOwnProperty(camera)) {
                            if (this.camera.id === cameras[camera].id) {
                                this.camera = cameras[camera];
                                break;
                            }
                        }
                    }
				},
				error => console.log(error) 
            );
            
        this.connection = this.imageService.getTheLastImageForCamera()
            .subscribe( 
                (data: {image, cameraId}) => {
                    if(data.cameraId === this.route.snapshot.params['id']) {
                        this.lastImage = data.image;
                    }
                },
                error => console.error(error)
            );
    }

    specFunc() {
        /*##################*/
		this.cameraService.getLayoutsWithSpecificCamera(this.camera)
			.subscribe(
				/*data => console.log(data),
				error => console.error(error)*/
			);
		/*##################*/
    }

}