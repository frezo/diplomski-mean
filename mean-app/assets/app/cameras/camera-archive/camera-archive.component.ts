import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from "@angular/router";

import { CameraService } from "../camera.service";
import { ImageService } from './image.service';
import { Camera } from "../camera.model";

@Component({
    selector: 'app-camera-archive',
    templateUrl: './camera-archive.component.html',
    styleUrls: ['./camera-archive.component.css']
})
export class CameraArchiveComponent implements OnInit {
    camera: Camera;

    slicedImages = [];
    images = [];
    showLoadingArea = false;
    isEmptyArray = false;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private cameraService: CameraService,
        private imageService: ImageService
    ) { }

    ngOnInit() {
        
        this.cameraService.getSingleCamera(this.route.snapshot.params['id'])
            .subscribe(
                (camera: Camera) => { 
                    this.camera = camera;
                }
            );
        
        this.route.params
            .subscribe(
                (params: Params) => {
                    this.cameraService.getSingleCamera(params['id'])
                    .subscribe(camera => this.camera = camera);
                }
            );
        
    }

    onChangedDates(startDateVal: Date, endDateVal: Date) {
         console.log('Start date: ', startDateVal);
        console.log('End date: ', endDateVal); 
        /* this.startDate.emit(startDateVal);
        this.endDate.emit(endDateVal); */
    }

    goToDetails() {
        this.router.navigate(['cameras', this.route.snapshot.params['id']])
    }

    public changedDateRange(dates) {
        this.slicedImages = [];
        this.showLoadingArea = true;
        this.isEmptyArray = false;
        this.imageService.getImagesForArchive(dates, this.route.snapshot.params['id'])
            .subscribe( images => {
                this.images = images;
                for (var i = 0; i < this.images.length; i += 61) {
                    this.slicedImages.push(this.images[i]);
                }
                if(this.slicedImages.length === 0) {
                    this.isEmptyArray = true;
                }
                this.showLoadingArea = false;
            }, error => console.error(error) );
    }
}