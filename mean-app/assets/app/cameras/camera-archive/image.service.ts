import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import * as io from "socket.io-client";

@Injectable()
export class ImageService {

    private imageURL = 'http://localhost:3000/image/'
    private headers = new Headers({'Content-Type': 'application/json'});
    private socket;

    constructor(
        private http: Http
    ) {
        this.socket = io('http://localhost:3001');
    }

    saveImages() {
        console.log('in save images');
        this.socket.emit('save image');
    } 

    getAllImagesForCamera(cameraId: string): Observable<any> {
        let observable = new Observable( observer => {
            this.socket = io('http://localhost:3001');
            this.socket.on('lastImage', (data) => {
                observer.next(data);
            });
            return () => {
                this.socket.disconnect();
            };
         });
        return observable;
    }

    getTheLastImageForCamera(): Observable<{}> {
        let observable = new Observable( observer => {
            this.socket = io('http://localhost:3001');
            this.socket.on('lastImage', (data) => {
                observer.next(data);
            });
            return () => {
                this.socket.disconnect();
            };
         });
        return observable;
    }

    saveImageForCamera(camera): Observable<Response> {
        const image = {
            name: new Date().getTime(),
            date: new Date(),
            camera: camera
        }
        const body = JSON.stringify(image);
        return this.http.post(this.imageURL, body, {headers: this.headers})
            .map( (response: Response) => response.json());
    }

    getImagesForArchive(rangeDates: any, cameraId: string): Observable<any> {
        return this.http.get(this.imageURL + rangeDates.startDate + '/' + rangeDates.endDate + '/' + cameraId)
            .map((response: Response) => response.json().obj)
            .catch((error: Response) => Observable.throw(error.json()));
    }
}