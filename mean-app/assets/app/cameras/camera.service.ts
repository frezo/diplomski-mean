import { Http, Response, Headers } from "@angular/http";
import { Injectable, EventEmitter } from "@angular/core";
import 'rxjs/Rx'; // Koristi nam za korištenje .map() metode
import { Observable } from "rxjs/Observable";
import { Subject } from "rxjs/Subject";

import { Camera } from "./camera.model";

@Injectable()
export class CameraService {
	private cameras: Camera[] = [];
	private camerasCopy = new Subject<Camera[]>();
	camerasCopy$ = this.camerasCopy.asObservable();

	constructor(private http: Http) {}

	addCamera(camera: Camera) {
		const body = JSON.stringify(camera);
		const headers = new Headers({'Content-Type': 'application/json'});
		return this.http.post('http://localhost:3000/camera', body, {headers: headers})
			.map((response: Response) => {
				const responseCamera = response.json().obj;
				const newCamera: Camera = new Camera(
					responseCamera.name,
					responseCamera.location,
					responseCamera.ip_address,
					responseCamera.image_format,
					responseCamera.camera_type,
					responseCamera._id,
				)
				this.cameras.push(newCamera);
				this.camerasCopy.next(this.cameras);
				return response.json();
			})
			.catch((error: Response) => Observable.throw(error.json()));
	}

	getCameras() {
		return this.http.get('http://localhost:3000/camera')
			.map((response: Response) => {
				const cameras = response.json().obj;
				let transformedCameras: Camera[] = [];
				for(let camera of cameras) {
					transformedCameras.push(
						new Camera(
							camera.name, 
							camera.location, 
							camera.ip_address, 
							camera.image_format, 
							camera.camera_type, 
							camera._id
						)
					);
				}
				this.cameras = transformedCameras;
				return transformedCameras;
			})
			.catch((error: Response) => Observable.throw(error.json()));
	}

	/* special functions */
	getAllCameras() {
		return this.http.get('http://localhost:3000/camera')
			.map((response: Response) => response.json())
			.catch((error: Response) => Observable.throw(error.json()));
	}
	getOneCamera(cameraId: string) {
		return this.http.get('http://localhost:3000/camera/' + cameraId)
			.map((response: Response) => response.json())
			.catch((error: Response) => Observable.throw(error.json()));
	}
	getLayoutsWithSpecificCamera(camera: any) {
		return this.http.get('http://localhost:3000/camera/test/' + camera.id)
			.map((response: Response) => response.json())
			.catch((error: Response) => Observable.throw(error.json()));
	}
	/* ##############*/

	getSingleCamera(id: any) {
		return this.http.get('http://localhost:3000/camera/' + id)
			.map((response: Response) => {
				const respCamera = response.json().obj;
				const newCamera: Camera = new Camera(
					respCamera.name,
					respCamera.location,
					respCamera.ip_address,
					respCamera.image_format,
					respCamera.camera_type,
					respCamera._id
				);
				return newCamera;
			})
			.catch((error: Response) => Observable.throw(error.json()));
	}

	updateCamera(oldCamera: Camera, editedCamera: Camera) {
        const body = JSON.stringify(editedCamera);
        const headers = new Headers({'Content-Type': 'application/json'});
        return this.http.patch('http://localhost:3000/camera/' + oldCamera.id, body, {headers: headers})
            .map((response: Response) => {
				for (var camera in this.cameras) {
					if (this.cameras.hasOwnProperty(camera)) {
						if (oldCamera.id === this.cameras[camera].id) {
							this.cameras[camera] = editedCamera;
							break;
						}
					}
				}
				this.camerasCopy.next(this.cameras);
				return response.json()
			})
            .catch((error: Response) => Observable.throw(error.json()));
    }

	deleteCamera(camera: Camera) {
		return this.http.delete('http://localhost:3000/camera/' + camera.id)
			.map((response: Response) => {
				this.cameras.splice(this.cameras.indexOf(camera), 1);
				this.camerasCopy.next(this.cameras);
				return response.json()
			})
			.catch((error: Response) => Observable.throw(error.json()));
	}
}