import { Component, OnInit, Output, EventEmitter, Input, SimpleChange } from '@angular/core';
import { NgForm, FormBuilder, FormGroup, Validators, FormControl } from "@angular/forms";

import { Camera } from "../camera.model";
import { CameraService } from "../camera.service";
import { UserService } from "../../users/user.service";

@Component({
    selector: 'app-camera-input',
    templateUrl: './camera-input.component.html'
})
export class CameraInputComponent implements OnInit {
	@Input() camera: Camera;
	@Output() isFormValid = new EventEmitter();
	cameraForm: FormGroup;
	valuesChanged = false;
	uniqueNames = [];

	imageFormats = ['JPEG (JPG)', 'PNG', 'GIF'];
	cameraTypes = ['Sony', 'Panasonic', 'Samsung', 'Canon'];

    constructor(
        private cameraService: CameraService,
		private userService: UserService,
		private formBuilder: FormBuilder
    ) { }

    ngOnInit() {
		this.initForm();
		this.watchForChanges();

		if(!!this.camera) {
			this.setValuesInForm();
		}

		this.cameraService.getCameras()
			.subscribe(
				(cameras: Camera[]) => {
					for (var camera in cameras) {
						if (cameras.hasOwnProperty(camera)) {
							this.uniqueNames.push(cameras[camera].name);
						}
					}
				}
			);

		this.cameraService.camerasCopy$
			.subscribe(
				(cameras: Camera[]) => {
					this.uniqueNames = [];
					for (var camera in cameras) {
						if (cameras.hasOwnProperty(camera)) {
							this.uniqueNames.push(cameras[camera].name);
						}
					}
					console.log(this.uniqueNames);
				},
				error => console.log(error) 
			);
	 }

	ngOnChanges(change: SimpleChange) {
        if ( !this.camera ) {
            this.camera = new Camera('','', '', '', '','');
		}
		if (!change.camera.firstChange) {
			this.cameraForm.reset();
			this.setValuesInForm();
		}
    }


	public initForm(): void {
		this.cameraForm = this.formBuilder.group({
			'name': 		['', [Validators.required, this.checkUniqueNames.bind(this), Validators.maxLength(20)]],
			'location': 	['', [Validators.required, Validators.maxLength(20)]],
			'ip_address': 	['', [Validators.required, Validators.pattern(/^(?!0)(?!.*\.$)((1?\d?\d|25[0-5]|2[0-4]\d)(\.|$)){4}$/i)]],
			'port': 		['', [Validators.maxLength(4), Validators.pattern(/^\d+$/i)]],
			'image_format': [''],
			'camera_type': 	['']
		});
	}

	public setValuesInForm(): void {
		this.cameraForm.controls['name'].patchValue(this.camera.name);
		this.cameraForm.controls['location'].setValue(this.camera.location);
		this.cameraForm.controls['ip_address'].setValue(this.extractIPAddress(this.camera.ip_address));
		this.cameraForm.controls['port'].setValue(this.extractPort(this.camera.ip_address));
		this.cameraForm.controls['image_format'].setValue(this.camera.image_format);
		this.cameraForm.controls['camera_type'].setValue(this.camera.camera_type);
	}

	public watchForChanges(): void {
		this.cameraForm.valueChanges
			.subscribe( (data) => {
				
				if (this.cameraForm.valid) {
					// Enable CREATE action only if form is valid
					this.isFormValid.emit(true);

					// Enable UPDATE action only if some value in form changes 
					if ((this.cameraForm.dirty || this.cameraForm.touched) && !!this.camera) {
						console.log('forma je: ', this.cameraForm.value);
						console.log('podaci su: ', data);
						if (data.name !== this.camera.name
						|| data.location !== this.camera.location
						|| this.composeIPAndPort() !== this.camera.ip_address
						|| data.image_format !== this.camera.image_format
						|| data.camera_type !== this.camera.camera_type) {
							this.valuesChanged = true;
						} else {
							this.valuesChanged = false;
						}
					}

				} else {
					this.isFormValid.emit(false);
					this.valuesChanged = false;
				}
			});
	}

	public extractIPAddress(value: string): string {
		return value.split(':').shift();
	}

	public extractPort(value: string): string {
		return (value.split(':').pop() === value) ? '' : value.split(':').pop();
	}

	public composeIPAndPort(): string {
		if(this.cameraForm.value.ip_address !== '') {
			return (!!this.cameraForm.value.port && this.cameraForm.value.port !== '')
				? this.cameraForm.value.ip_address + ':' + this.cameraForm.value.port
				: this.cameraForm.value.ip_address 
		}
		return '';
	}

	// Custom validation for unique names
	public checkUniqueNames(control: FormGroup): {[s: string]: boolean} {
		if (this.uniqueNames.indexOf(control.value) !== -1
			&& control.value !== this.camera.name) {
			return { 'nameIsForbidden': true }
		}
		return null;
	}

    public onSubmit(): void {
		// Create
		if (this.valuesChanged) {
			const camera = new Camera(
				this.cameraForm.value.name, 
				this.cameraForm.value.location, 
				this.composeIPAndPort(), 
				this.cameraForm.value.image_format, 
				this.cameraForm.value.camera_type
			);
			console.log('Sprema se kamera: ', camera);
			this.cameraService.addCamera(camera)
				.subscribe(
					data => console.log(data), // success function
					error => console.error(error) // error funcion
				);
			this.cameraForm.reset();
		}
	}

	public onUpdate(): void {
		// Update
		if (this.valuesChanged) {
			const editedCamera = new Camera(
				this.cameraForm.value.name,
				this.cameraForm.value.location,
				this.composeIPAndPort(),
				this.cameraForm.value.image_format,
				this.cameraForm.value.camera_type,
				this.camera.id
			);
			
			this.cameraService.updateCamera(this.camera, editedCamera)
				.subscribe(
					result => {
						this.camera = editedCamera;
						this.updateWhachingCameraList();
					},
					error => console.log(error)
				);
		}
	}

	public updateWhachingCameraList(): void {
		const userId = this.userService.getStoredUserId();
		if(userId) {
			this.userService.getMappedUserWithCameras(userId)
				.subscribe(
					data => {
                        console.log(data.obj);
                    },
					error => console.error(error)
				);
		}
	}

	public isEmpty(obj): boolean {
		for(var key in obj) {
			if(obj.hasOwnProperty(key))
				return false;
		}
		return true;
	}

}