import { Component, OnInit } from "@angular/core";

import { Camera } from "../camera.model";
import { CameraService } from "../camera.service";
import { UserService } from "../../users/user.service";

@Component({
	selector: 'app-camera-list',
	templateUrl: './camera-list.component.html'
})

export class CameraListComponent implements OnInit {
	cameras: Camera[];
	user;

    constructor(
		private cameraService: CameraService,
		private userService: UserService
	) {
		this.userService.watchCameras$
			.subscribe(
				data => {
					this.cameras = data;
				},
				error => console.log(error) 
			);

		this.userService.loggedInUser$
			.subscribe(
				data => this.user = data,
				error => console.log(error)
			);
	}

    ngOnInit() {
		const userId = this.userService.getStoredUserId();
		if(userId) {
			this.userService.getLoggedInUser(userId)
				.subscribe(
					user => {
						this.user = user.obj;
						this.getAvailableCamerasForLoggedInUser();
					},
					error => console.error(error)
				);
		}
    }

	getAvailableCamerasForLoggedInUser() {
		this.userService.getMappedUserWithCameras(this.user._id)
			.subscribe(
				data => {
					this.cameras = data.obj.checkedCameras;
				},
				error => console.error(error)
			);
	}

}