import { Routes } from '@angular/router';

import { CameraDetailComponent } from "./camera-detail/camera-detail.component";
import { CameraArchiveComponent } from "./camera-archive/camera-archive.component";
import { CameraListComponent } from "./camera-list/camera-list.component";

export const CAMERA_ROUTES: Routes = [
    { path: '', component: CameraListComponent },
    { path: ':id', component: CameraDetailComponent },
    { path: ':id/archive', component: CameraArchiveComponent }
];