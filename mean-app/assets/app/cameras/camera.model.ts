export class Camera {

    constructor(
        public name: string, 
        public location: string, 
        public ip_address: string, 
        public image_format?: string, 
        public camera_type?: string,
        public id?: string
    ) { }

}
