import { Component, Input, OnInit, OnDestroy } from "@angular/core";

import { CameraService } from "./camera.service";
import { ImageService } from './camera-archive/image.service';
import { Camera } from "./camera.model";

@Component({
    selector: 'app-camera',
    templateUrl: './camera.component.html'
})

export class CameraComponent implements OnInit, OnDestroy {
    @Input() camera: Camera;

    connection;
    theLastImage;

    constructor(
        private cameraService: CameraService,
        private imageService: ImageService
    ) {}

    ngOnInit() {
        this.connection = this.imageService.getTheLastImageForCamera()
            .subscribe( 
                (data: {image, cameraId}) => {
                    if(data.cameraId === this.camera['_id']) {
                        this.theLastImage = data.image;
                    }
                },
                error => console.error(error)
            );
    }

    ngOnDestroy() {
        /* this.connection.unsubscribe(); */
    }

    onDelete() {
        this.cameraService.deleteCamera(this.camera)
            .subscribe(
                result => console.log(result)
            );
    }
}