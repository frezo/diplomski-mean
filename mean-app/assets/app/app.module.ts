import { ImageService } from './cameras/camera-archive/image.service';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";

import { AppComponent } from "./app.component";
import { NavigationTopComponent } from "./navigations/navigation-top.component";
import { NavigationSideComponent } from "./navigations/navigation-side.component";
import { PageHeadingComponent } from "./shared/page-heading/page-heading.component";
import { CameraComponent } from "./cameras/camera.component";
import { CameraListComponent } from "./cameras/camera-list/camera-list.component";
import { CameraDetailComponent } from "./cameras/camera-detail/camera-detail.component";
import { CameraInputComponent } from "./cameras/camera-input/camera-input.component";
import { CameraArchiveComponent } from "./cameras/camera-archive/camera-archive.component";
import { AuthenticationComponent } from "./users/authentication.component";
import { SignupComponent } from "./users/signup/singup.component";
import { LoginComponent } from "./users/login/login.component";
import { PageNotFoundComponent } from "./shared/page-not-found/page-not-found.component";
import { LayoutComponent } from "./shared/layout/layout.component";
import { ProfileComponent } from "./users/profile/profile.component";
import { ManageComponent } from "./users/manage/manage.component";
import { UserInputComponent } from "./users/manage/user-input/user-input.component";
import { UrlDashPipe } from "./shared/url-dash.pipe";

import { routing } from "./app.routing";
import { CameraService } from "./cameras/camera.service";
import { UserService } from "./users/user.service";
import { NewUserComponent } from "./users/manage/new-user/new-user.component";
import { SearchUsersPipe } from "./shared/search-users.pipe";


import { Daterangepicker } from 'ng2-daterangepicker';
import { DaterangepickerComponent } from './shared/date-range-picker/date-range-picker.component';


import 'jquery/dist/jquery.min.js'
import 'bootstrap/dist/js/bootstrap.js';
import '../../node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js';


@NgModule({
    declarations: [
        AppComponent,
        NavigationTopComponent,
        NavigationSideComponent,
        CameraComponent,
        PageHeadingComponent,
        CameraInputComponent,
        CameraListComponent,
        CameraDetailComponent,
        CameraArchiveComponent,
        AuthenticationComponent,
        PageNotFoundComponent,
        SignupComponent,
        LoginComponent,
        LayoutComponent,
        ProfileComponent,
        ManageComponent,
        UserInputComponent,
        NewUserComponent,
        SearchUsersPipe,
        UrlDashPipe,
        DaterangepickerComponent
    ],
    imports: [
        BrowserModule, 
        BrowserAnimationsModule, 
        FormsModule, 
        ReactiveFormsModule,
        HttpModule,
        routing,
        Daterangepicker
    ],
    providers: [CameraService, UserService, ImageService],
    bootstrap: [AppComponent]
})
export class AppModule {

}