import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

import { UserService } from "../users/user.service";
import { User } from "../users/user.model";

@Component({
	selector: 'app-top-navigation',
	templateUrl: './navigation-top.component.html'
})

export class NavigationTopComponent implements OnInit {
	user: User;

	constructor(
		private router: Router,
		private userService: UserService
	) {
		this.userService.loggedInUser$
			.subscribe(
				data => this.user = data,
				error => console.log(error)
			);
	}

	ngOnInit() {
		const userId = this.userService.getStoredUserId();
		if(userId) {
			this.userService.getSingleUser(userId)
				.subscribe(
					user => this.user = user.obj,
					error => {
						console.error(error);
						if (error.error.name === 'TokenExpiredError') {
							console.log('istekao token!');
							this.userService.logout();
							this.router.navigateByUrl('/login');
						}
					}
				);
		}
	}

	onLogout() {
		this.userService.logout();
		this.router.navigateByUrl('/login');
	}
}