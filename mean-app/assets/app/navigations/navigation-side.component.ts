import { Component, OnInit, OnChanges } from "@angular/core";
import { Router } from "@angular/router";

import { Camera } from "../cameras/camera.model";
import { CameraService } from "../cameras/camera.service";
import { UserService } from "../users/user.service";

@Component({
	selector: 'app-side-navigation',
	templateUrl: './navigation-side.component.html'
})

export class NavigationSideComponent implements OnInit {
	user;
	cameras: Camera[];

    constructor(
		private cameraService: CameraService,
		private userService: UserService,
		private router: Router
	) {

		this.userService.watchCameras$
			.subscribe(
				data => this.cameras = data,
				error => console.log(error) 
			);

		this.userService.loggedInUser$
			.subscribe(
				data => this.user = data,
				error => console.log(error)
			);
	}

    ngOnInit() {
		const userId = this.userService.getStoredUserId();
		if(userId) {
			this.userService.getLoggedInUser(userId)
				.subscribe(
					user => {
						this.user = user.obj;
						this.getAvailableCamerasForLoggedInUser();
					},
					error => console.error(error)
				);
		}
    }

	getAvailableCamerasForLoggedInUser() {
		this.userService.getMappedUserWithCameras(this.user._id)
			.subscribe(
				data => {
					this.cameras = data.obj.checkedCameras;
				},
				error => console.error(error)
			);
	}
	
}