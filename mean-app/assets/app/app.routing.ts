import { Routes, RouterModule } from "@angular/router";

import { AppComponent } from "./app.component";
import { AuthenticationComponent } from "./users/authentication.component";
import { CameraDetailComponent } from "./cameras/camera-detail/camera-detail.component";
import { CameraListComponent } from "./cameras/camera-list/camera-list.component";
import { CameraArchiveComponent } from "./cameras/camera-archive/camera-archive.component";
import { PageNotFoundComponent } from "./shared/page-not-found/page-not-found.component";
import { LoginComponent } from "./users/login/login.component";
import { LayoutComponent } from "./shared/layout/layout.component";

import { CAMERA_ROUTES } from "./cameras/camera.routing";
import { USER_ROUTES } from "./users/user.routing";

const APP_ROUTES : Routes = [
	{ path: '', redirectTo: 'cameras', pathMatch: 'full' },
	{ path: 'cameras', component: LayoutComponent, children: CAMERA_ROUTES },
	{ path: 'users', component: LayoutComponent, children: USER_ROUTES },
	{ path: 'login', component: LoginComponent },
	{ path: '**', component: PageNotFoundComponent }
];

export const routing = RouterModule.forRoot(APP_ROUTES);