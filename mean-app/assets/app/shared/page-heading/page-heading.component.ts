import { UserService } from './../../users/user.service';
import { Router } from '@angular/router';
import { Component, Input, OnInit } from "@angular/core";
import { NgForm } from "@angular/forms";
import { Location } from '@angular/common';

import { CameraService } from "../../cameras/camera.service";
import { Camera } from "../../cameras/camera.model";

@Component({
	selector: 'app-page-heading',
	templateUrl: './page-heading.component.html',
	styleUrls: ['./page-heading.component.css']
})

export class PageHeadingComponent implements OnInit {
	@Input() title: string;
	@Input() description: string;
	@Input() notFrontPage: boolean;
	@Input() camera: Camera;

	private formValid: boolean = false;
	showDeleteModal = false;
	user;

	constructor(
		private cameraService: CameraService,
		private userService: UserService,
		private location: Location,
		private router: Router
	) {
		this.userService.loggedInUser$
			.subscribe(
				data => this.user = data,
				error => console.log(error)
			);
	}

	ngOnInit() {
		if (!this.title) { this.title = 'Kamere'; }
		if (!this.description) { this.description = 'Pregled uživo'; }
		this.userService.getLoggedInUser(this.userService.getStoredUserId())
			.subscribe(
				user => this.user = user.obj,
				error => console.error(error)
			);
	}

	isFormValid() {
		if ( this.formValid ) {
			return true;
		} else {
			return false;
		}
	}

	goBack(): void {
		this.location.back();
	}

	onDelete() {
        this.cameraService.deleteCamera(this.camera)
            .subscribe(
                success => {
                    console.log(success)
                    this.router.navigate(['/']);
                },
                error => console.log(error)
            );
    }
}