import { Pipe, PipeTransform } from '@angular/core';
import { User } from "../users/user.model";

@Pipe({
  name: 'searchUsers',
  pure: false
})
export class SearchUsersPipe implements PipeTransform {

    transform(items: User[], filter: string): User[] {
        if (!items || !filter) {
            return items;
        }
        // filter items array, items which match and return true will be kept, false will be filtered out
        return items.filter((item: User) => this.applyFilters(item, filter));
    }

    applyFilters(user: User, filter: string): boolean {
        if (user['firstName'].toLowerCase().indexOf(filter.toLowerCase()) === -1 &&
            user['lastName'].toLowerCase().indexOf(filter.toLowerCase()) === -1) {
            return false;
        }
        return true;
    }

}
