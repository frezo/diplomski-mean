import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'urlDash'})
export class UrlDashPipe implements PipeTransform {
    transform(value: any): any {
        return value.replace(' ', '-');
    }
}