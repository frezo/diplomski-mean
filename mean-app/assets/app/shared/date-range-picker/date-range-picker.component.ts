import {Component, EventEmitter, Output} from '@angular/core';

@Component({
  selector: 'app-daterangepicker',
  templateUrl: './date-range-picker.component.html'
})

export class DaterangepickerComponent {

  @Output() dateRangeDates: EventEmitter<any> = new EventEmitter();

  public daterange: any = {};
  
  public options: any = {
      'locale': { 
        format: 'DD/MM/YYYY HH:mm',
        'daysOfWeek': ['Ne','Po','Ut','Sr','Če','Pe','Su'],
        'monthNames': [
            'Siječanj',
            'Veljača',
            'Ožujak',
            'Travanj',
            'Svibanj',
            'Lipanj',
            'Srpanj',
            'Kolovoz',
            'Rujan',
            'Listopad',
            'Studeni',
            'Prosinac'
        ],
        'firstDay': 1,
        'applyLabel': 'Spremi',
        'cancelLabel': 'Odustani',
      },
      'alwaysShowCalendars': false,
      'timePicker': true,
      'timePicker24Hour': true,
      'dateLimit': {
        'days': 7
      },
      'maxDate': new Date()
  };

  public selectedDate(value: any, datepicker?: any) {

    // any object can be passed to the selected event and it will be passed back here
    datepicker.start = value.start;
    datepicker.end = value.end;

    // or manupulat your own internal property
    this.daterange.start = value.start;
    this.daterange.end = value.end;
    this.daterange.label = value.label;

    this.dateRangeDates.emit({
      startDate: value.picker.startDate._d, 
      endDate: value.picker.endDate._d
    });
  } 

}
