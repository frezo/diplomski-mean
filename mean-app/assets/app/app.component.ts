import { ImageService } from './cameras/camera-archive/image.service';
import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Router } from "@angular/router";

import { User } from "./users/user.model";

import { UserService } from "./users/user.service";

@Component({
    selector: 'app-content',
    templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {
    users: User[] = [];
    user;

    constructor(
        private userService: UserService,
        private imageService: ImageService,
        private router: Router
    ) { }
    
    ngOnInit() {
        const userId = this.userService.getStoredUserId();
		if(userId) {
			this.userService.getSingleUser(userId)
				.subscribe(
					user => this.user = user.obj,
					error => {
						console.error(error);
						if (error.error.name === 'TokenExpiredError') {
							console.log('istekao token!');
							this.userService.logout();
							this.router.navigateByUrl('/login');
						}
					}
				);
		}

        if(!this.userService.isLoggedIn()) {
            console.log('Niste prijavljeni!');
            this.router.navigateByUrl('/login');
        }

        this.imageService.saveImages();
    }
}