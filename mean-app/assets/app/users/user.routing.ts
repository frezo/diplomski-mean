import { Routes } from '@angular/router';

import { ProfileComponent } from "./profile/profile.component";
import { ManageComponent } from "./manage/manage.component";
import { UserInputComponent } from "./manage/user-input/user-input.component";
import { NewUserComponent } from "./manage/new-user/new-user.component";

export const USER_ROUTES: Routes = [
    { path: 'profile', component: ProfileComponent },
    { path: 'manage', component: ManageComponent, children: [
        { path: 'new', component: NewUserComponent },
        { path: ':id', component: UserInputComponent },
    ] },
    
];