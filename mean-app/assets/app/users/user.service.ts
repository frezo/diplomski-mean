import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Subject } from "rxjs/Subject";

import { User } from "./user.model";
import { Camera } from "../cameras/camera.model";
import { CameraService } from "../cameras/camera.service";

@Injectable()
export class UserService {

    url = 'http://localhost:3000';
    headers = new Headers({'Content-Type': 'application/json'});
    token = localStorage.getItem('token') ? localStorage.getItem('token') : '';

    privateUser;
    private loggedInUser = new Subject<any>();
    loggedInUser$ = this.loggedInUser.asObservable();

    users: User[];
    private usersList = new Subject<User[]>();
	usersList$ = this.usersList.asObservable();

    private watchCameras = [];
	private watchCamerasObs = new Subject<any>();
	watchCameras$ = this.watchCamerasObs.asObservable();


    constructor(
        private http: Http,
        private cameraService: CameraService
    ) { }

    addUser(user: User) {
        console.log(user);
        const body = JSON.stringify(user);
		return this.http.post(this.url + '/user/signup' + '?token=' + this.token, body, {headers: this.headers})
            .map((response: Response) => {
                this.users.push(response.json().obj);
                this.usersList.next(this.users);
                return response.json();
            })
            .catch((error: Response) => Observable.throw(error));
    }

    deleteUser(deletedUser: any) {
		return this.http.delete(this.url + '/user/' + deletedUser._id + '?token=' + this.token)
			.map((response: Response) => {
                let userIndex = this.users.findIndex(user => user['_id'] == deletedUser._id);
                if (userIndex !== -1) {
                    this.users.splice(userIndex, 1);
                }
                console.log('index', userIndex);
                console.log('lista', this.users);
                this.usersList.next(this.users);
				return response.json();
			})
			.catch((error: Response) => Observable.throw(error.json()));
    }
    
    updateUserInfo(updatedUser: any, isLoggedInUser: boolean) {
        const body = JSON.stringify(updatedUser);
		return this.http.patch(this.url + '/user/' + updatedUser._id + '?token=' + this.token, body, {headers: this.headers})
            .map((response: Response) => {
                let userIndex = this.users.findIndex(user => user['_id'] == updatedUser._id);
                this.users[userIndex] = updatedUser;
                if(isLoggedInUser) {
                    this.privateUser = response.json().obj;
                    this.loggedInUser.next(this.privateUser);
                }
				this.usersList.next(this.users);
				return response.json();
            })
            .catch((error: Response) => Observable.throw(error));
    }

    updateUserPassword(userId: string, oldPass: string, newPass: string) {
        const body = JSON.stringify({
            oldPassword: oldPass,
            newPassword: newPass
        });
		return this.http.patch(this.url + '/user/password/' + userId + '?token=' + this.token, body, {headers: this.headers})
            .map((response: Response) => {
                for (var user in this.users) {
					if (this.users.hasOwnProperty(user)) {
						this.users[user] = response.json().obj;
					}
				}
                this.privateUser = response.json().obj;
                this.loggedInUser.next(this.privateUser);
				this.usersList.next(this.users);
				return response.json();
            })
            .catch((error: Response) => Observable.throw(error));
    }

    firstLogin(user: User) {
        const body = JSON.stringify(user);
		return this.http.post(this.url + '/user/first-login', body, {headers: this.headers})
            .map((response: Response) => response.json())
            .catch((error: Response) => Observable.throw(error));
    }

    getUsersExceptOne(userID: string) {
        return this.http.get(this.url + '/user/not/' + userID + '?token=' + this.token)
            .map((response: Response) => {
                this.users = response.json().obj;
                this.usersList.next(this.users);
                return response.json();
            })
            .catch((error: Response) => Observable.throw(error.json()));
    }

    getSingleUser(id: any) {
        return this.http.get(this.url + '/user/' + id + '?token=' + this.token)
            .map((response: Response) => response.json())
            .catch((error: Response) => Observable.throw(error.json()));
    }

    getLoggedInUser(userID: string) {
        return this.http.get(this.url + '/user/' + userID + '?token=' + this.token)
            .map((response: Response) =>{
                this.privateUser = response.json().obj;
                this.loggedInUser.next(this.privateUser);
                return response.json();
            })
            .catch((error: Response) => Observable.throw(error.json()));
    }

    login(user: User) {
        const body = JSON.stringify(user);
		return this.http.post(this.url + '/user/login', body, {headers: this.headers})
            .map((response: Response) => {
                this.privateUser = response.json().user;
                this.loggedInUser.next(this.privateUser);
                this.token = response.json().token;
                return response.json();
            })
            .catch((error: Response) => Observable.throw(error));
    }

    logout() {
        localStorage.clear();
        this.privateUser = null;
        this.loggedInUser.next(this.privateUser);
    }

    isLoggedIn() {
        return localStorage.getItem('token') !== null;
    }

    getStoredUserId() {
        return localStorage.getItem('userId');
    }



    initMapUserWithCameras(user: User, cameras: Camera[]) {
        const myData = {
            user: user,
            cameras: cameras
        };
        const body = JSON.stringify(myData);
        console.log(myData);
        return this.http.post(this.url + '/user-camera/init', body, {headers: this.headers})
            .map((response: Response) => response.json())
            .catch((error: Response) => Observable.throw(error));
    }

    mapUserWithCameras(camera: Camera) {
        const body = JSON.stringify(camera);
        console.log(body);
        return this.http.post(this.url + '/user-camera' + '?token=' + this.token, body, {headers: this.headers})
            .map((response: Response) => response.json())
            .catch((error: Response) => Observable.throw(error));
    }

    getMappedUserWithCameras(userID: string) {
        return this.http.get(this.url + '/user-camera/' + userID + '?token=' + this.token)
            .map((response: Response) => {
                this.watchCameras = response.json().obj.checkedCameras;
                this.watchCamerasObs.next(this.watchCameras);
                return response.json();
            })
            .catch((error: Response) => Observable.throw(error));
    }

    getCheckedCameraForUser(userID: string) {
        return this.http.get(this.url + '/user-camera/' + userID + '?token=' + this.token)
            .map((response: Response) => response.json())
            .catch((error: Response) => Observable.throw(error));
    }

    updateMappedUserWithCameras(cameraId: string, checked: boolean) {
        const body = JSON.stringify({
            cameraId: cameraId,
            checked: checked
        });
        return this.http.patch(this.url + '/user-camera' + '?token=' + this.token, body, {headers: this.headers})
            .map((response: Response) => {
                this.watchCameras = response.json().obj.checkedCameras;
                this.watchCamerasObs.next(this.watchCameras);
                return response.json();
            })
            .catch((error: Response) => Observable.throw(error));
    }

    updateUserAvailableCameras(userID: string, camera: any, checked: boolean) {
        const body = JSON.stringify({
            camera: camera,
            checked: checked
        });
        return this.http.patch(this.url + '/user-camera/' + userID + '?token=' + this.token, body, {headers: this.headers})
            .map((response: Response) => response.json())
            .catch((error: Response) => Observable.throw(error));
    }


    deleteMappedUserWithCameras() {
        return this.http.delete(this.url + '/user-camera' + '?token=' + this.token)
            .map((response: Response) => response.json())
            .catch((error: Response) => Observable.throw(error));
    }
}