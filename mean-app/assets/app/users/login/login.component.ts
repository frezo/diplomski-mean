import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router } from "@angular/router";

import { User } from "../user.model";
import { UserService } from "../user.service";
import { CameraService } from "../../cameras/camera.service";
import { Camera } from "../../cameras/camera.model";

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
	user = new User('', '');
	loggedInUser;
	cameras: Camera[];

	loginForm: FormGroup;
	username: FormControl;
	password: FormControl;

	constructor(
		private userService: UserService,
		private cameraService: CameraService,
		private router: Router,
		private fb: FormBuilder
	) {	}

	ngOnInit() {
		this.initForm();
	}

	public initForm() {
		this.username = new FormControl('', [Validators.required, Validators.maxLength(20)]);
		this.password = new FormControl('', [Validators.required, Validators.maxLength(20), Validators.minLength(6)]);
		this.loginForm = this.fb.group({
			'username': this.username,
			'password': this.password
		});
	}

	onSubmit() {
		if (this.username.value === 'admin0' && this.password.value === 'admin123') {
			this.user.username = this.username.value;
			this.user.password = this.password.value;
			this.user.level = 1;
			this.userService.firstLogin(this.user)
				.subscribe(
					data => {
						this.loggedInUser = data.obj;
						this.getAllCameras();
					},
					error => console.error(error)
				);
		} else {
			this.login();
		}
	}

	getAllCameras() {
		this.cameraService.getAllCameras()
			.subscribe(data => {
				this.cameras = data.obj;
				this.initMapUserCameraSchema();
			});
	}

	initMapUserCameraSchema() {
		console.log(this.user);
		this.userService.initMapUserWithCameras(this.loggedInUser, this.cameras)
			.subscribe(data => {
				console.log(data);
				this.login();
			});
	}

	login() {
		const user = new User(this.username.value, this.password.value);
		this.userService.login(user)
			.subscribe(
				data => {
					localStorage.setItem('token', data.token);
					localStorage.setItem('userId', data.user._id);
					this.router.navigateByUrl('/');
				},
				error => console.error(error)
			);
	}

}