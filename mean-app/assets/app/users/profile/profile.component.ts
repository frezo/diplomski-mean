import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

import { User } from "../user.model";
import { CameraService } from "../../cameras/camera.service";
import { UserService } from "../user.service";
import { Camera } from "../../cameras/camera.model";

@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html'
})
export class ProfileComponent implements OnInit {
    profileForm: FormGroup;
    loginInfoForm: FormGroup;
    user: User;
    userId: string;
    cameras: Camera[];
    checkedCameras = [];
    levels = ['Nadzornik', 'Admin'];

    constructor(
        private formBuilder: FormBuilder,
        private cameraService: CameraService,
        private userService: UserService
    ) { 
        this.profileForm = this.formBuilder.group({
			'firstName': ['', [Validators.minLength(3), Validators.maxLength(20)]],
			'lastName': ['', [Validators.minLength(3), Validators.maxLength(20)]],
			'email': ['', Validators.email]
		});
        this.loginInfoForm = new FormGroup({
            'oldPassword': new FormControl('', [Validators.required, Validators.minLength(6)]),
            'newPassword': new FormControl('', [Validators.required, Validators.minLength(6)])
        });
    }

    ngOnInit() { 
        this.user = new User('admin', 'root', 'Admin', 'User', 'admin@admin.com', 1);
        const userId = this.userService.getStoredUserId();
		if(userId) {
			this.userService.getSingleUser(userId)
				.subscribe(
					user => {
                        this.user = user.obj;
                        this.getUserAndCameras(userId);
                    },
					error => console.error(error)
				);
		}
    }

    getUserAndCameras(userId) {
        this.userService.getMappedUserWithCameras(userId)
            .subscribe(data => {
                this.userId = data.obj.user;
                this.cameras = data.obj.availableCameras;
                this.checkedCameras = data.obj.checkedCameras;
            });
    }

    onUserInfoSubmit() {
        this.userService.updateUserInfo(this.user, true)
            .subscribe(
                data => {
                    this.profileForm.reset();
                    this.profileForm.controls['firstName'].setValue(data.obj.firstName);
                    this.profileForm.controls['lastName'].setValue(data.obj.lastName);
                    this.profileForm.controls['email'].setValue(data.obj.email);
                },
                error => console.error(error)
            );
    }

    onUserPasswordChange() {
        const oldPass = this.loginInfoForm.controls['oldPassword'].value;
        const newPass = this.loginInfoForm.controls['newPassword'].value;
        this.userService.updateUserPassword(this.userId, oldPass, newPass)
            .subscribe(
                data => {
                    this.loginInfoForm.reset();
                },
                error => console.error(error)
            )
    }

    onClickedCheckbox(camera, event) {
        this.userService.updateMappedUserWithCameras(event.target.value, event.target.checked)
            .subscribe(res => console.log(res));
    }

    isCameraChecked(cameraId) {
        for (var camera in this.checkedCameras) {
            if (this.checkedCameras.hasOwnProperty(camera)) {
                var element = this.checkedCameras[camera];
                if (element._id === cameraId) {
                    return true;
                }
            }
        }
        return false;
    }
}