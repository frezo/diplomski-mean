import { Component, OnInit } from '@angular/core';

import { UserService } from "../user.service";

import { User } from "../user.model";

@Component({
    selector: 'app-manage',
    templateUrl: './manage.component.html',
    styleUrls: ['./manage.component.css']
})
export class ManageComponent implements OnInit {
    users: User[];
    searchInput = '';
    searchUser = new User('', '', '');

    constructor(
        private userService: UserService
    ) { 
        this.userService.getUsersExceptOne(this.userService.getStoredUserId())
            .subscribe(
                data => this.users = data.obj,
                error => console.error(error)
            );
        
        this.userService.usersList$
			.subscribe(
				data => this.users = data,
				error => console.log(error) 
			);
    }

    ngOnInit() { 
        
    }
}