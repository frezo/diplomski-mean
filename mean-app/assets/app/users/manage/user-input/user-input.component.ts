import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from "@angular/router";

import { UserService } from "../../user.service";
import { CameraService } from "../../../cameras/camera.service";

import { User } from "../../user.model";
import { Camera } from "../../../cameras/camera.model";

@Component({
    selector: 'app-user-input',
    templateUrl: './user-input.component.html',
    styleUrls: ['./user-input.component.css']
})

export class UserInputComponent implements OnInit {
    userForm: FormGroup;
    user = new User('', '');
    userId: string;
    cameras = [];
    checkedCameras = [];

    levels = ['Nadzornik', 'Admin'];

    constructor(
        private route: ActivatedRoute,
        private userService: UserService,
        private cameraService: CameraService,
        private router: Router
    ) { 
        const routeID =  this.route.snapshot.params['id'];
        if (routeID !== 'new') {
            this.userService.getSingleUser(this.route.snapshot.params['id'])
                .subscribe(user => {
                                this.user = user.obj;
                                this.userId = user.obj._id;
                                /*this.updateCheckedCameras(user.obj._id);*/
                            });

            this.route.params
                .subscribe(
                    (params: Params) => {
                        this.userService.getSingleUser(params['id'])
                            .subscribe(user => {
                                this.user = user.obj;
                                this.userId = user.obj._id;
                                this.updateCheckedCameras(this.userId);
                            });
                    }
                );

            this.cameraService.getAllCameras()
                .subscribe(cameras => this.cameras = cameras.obj);

        }

        this.userForm = new FormGroup({
            'firstName': new FormControl('', [Validators.required, Validators.maxLength(20)]),
            'lastName': new FormControl( '', [Validators.required, Validators.maxLength(20)]),
            'email': new FormControl('', Validators.email),
            'level': new FormControl('')
        });
    }

    ngOnInit() { 

    }

    updateCheckedCameras(userID) {
        this.userService.getCheckedCameraForUser(userID)
            .subscribe(data => {
                /*this.userId = data.obj.user;
                this.cameras = data.obj.availableCameras;*/
                this.checkedCameras = data.obj.availableCameras;
            });
    }

    isCameraChecked(cameraId) {
        for (var camera in this.checkedCameras) {
            if (this.checkedCameras.hasOwnProperty(camera)) {
                var element = this.checkedCameras[camera];
                if (element._id === cameraId) {
                    return true;
                }
            }
        }
        return false;
    }

    updateCheckedCamerasList(camera, event) {
        this.userService.updateUserAvailableCameras(this.userId, camera, event.target.checked)
            .subscribe(
                data => {
                    if (event.target.checked) {
                        this.checkedCameras.push(camera);
                    } else {
                        this.checkedCameras = this.checkedCameras.filter(function(el) { 
                            return el._id != event.target.value; 
                        });
                    }
                    console.log(this.checkedCameras);
                    console.log(data);
                },
                error => console.error(error)
            );
    }

    onSubmit() {
        this.user.firstName = this.userForm.value.firstName;
        this.user.lastName = this.userForm.value.lastName;
        this.user.email = this.userForm.value.email;
        this.userService.updateUserInfo(this.user, false)
            .subscribe(
                data => {
                    this.userForm.reset();
                    this.userForm.controls['firstName'].setValue(data.obj.firstName);
                    this.userForm.controls['lastName'].setValue(data.obj.lastName);
                    this.userForm.controls['email'].setValue(data.obj.email);
                },
                error => console.error(error)
            );
    }

    public onUserDelete() {
        this.userService.deleteUser(this.user)
            .subscribe( deletedUser => {
                this.router.navigate(['/users/manage']);
            },
            error => console.error(error)
        );
    }
}