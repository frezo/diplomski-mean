import { Component, OnInit } from '@angular/core';
import { User } from "../../user.model";
import { FormGroup, FormControl, Validators } from "@angular/forms";

import { UserService } from "../../user.service";
import { CameraService } from "../../../cameras/camera.service";

@Component({
    selector: 'app-new-user',
    templateUrl: './new-user.component.html'
})
export class NewUserComponent implements OnInit {
    newUserForm: FormGroup;
    cameras = [];
    checkedCameras = [];
    newUser;
    user = new User('', '', '', '', '', 0);
    levels = ['Nadzornik', 'Admin'];

    constructor(
        private userService: UserService,
        private cameraService: CameraService
    ) { 
        this.newUserForm = new FormGroup({
            'firstName': new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(20)]),
            'lastName': new FormControl( '', [Validators.required, Validators.minLength(3), Validators.maxLength(20)]),
            'email': new FormControl('', Validators.email),
            'level': new FormControl('')
        });

        this.cameraService.getAllCameras()
            .subscribe(cameras => this.cameras = cameras.obj);
    }

    ngOnInit() { 
        
    }

    onSubmit() {
        this.user.username = this.user.firstName.substr(0, 3).toLowerCase() + this.user.lastName.substr(0, 3).toLowerCase() + '0';
        this.user.password = 'dalmacija';
        this.user.firstName = this.user.firstName.charAt(0).toUpperCase() + this.user.firstName.slice(1);
        this.user.lastName = this.user.lastName.charAt(0).toUpperCase() + this.user.lastName.slice(1);

        this.userService.addUser(this.user)
            .subscribe(
                response => {
                    this.newUser = response.obj;
                    this.newUserForm.reset();
                    if (this.newUser.level === 1) {
                        this.cameraService.getAllCameras()
                            .subscribe(
                                cameras => {
                                    this.checkedCameras = cameras.obj;
                                    this.initMapUserCameraSchema();
                                },
                                error => console.log(error)
                            );
                    } else {
                        this.initMapUserCameraSchema();
                    }
                },
                error => console.error(error)
            );
    }

    initMapUserCameraSchema() {
		this.userService.initMapUserWithCameras(this.newUser, this.checkedCameras)
			.subscribe(
                data => {
                    console.log(data);
                },
                error => console.error(error)
            );
	}

    onCheckedCheckbox(event) {
        this.cameraService.getOneCamera(event.target.value)
            .subscribe(
                camera => this.updateCheckedCamerasList(event, camera.obj),
                error => console.error(error)
            );
    }

    updateCheckedCamerasList(event, camera) {
        if (event.target.checked) {
            this.checkedCameras.push(camera);
        } else {
            this.checkedCameras = this.checkedCameras.filter(function(el) { 
                return el._id != event.target.value; 
            });
        }
    }
}