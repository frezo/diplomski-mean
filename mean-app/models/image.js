var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
    name: {type: String, required: true},
    date: {type: Date, default: Date.now},
    imgUrl: {type: String},
    camera: {type: Schema.Types.ObjectId, ref: 'Camera'}
});

module.exports = mongoose.model('Image', schema);