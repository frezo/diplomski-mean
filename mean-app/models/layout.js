var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
    user: {type: Schema.Types.ObjectId, ref: 'User', required: true},
    availableCameras: [{type: Schema.Types.ObjectId, ref: 'Camera'}],
    checkedCameras: [{type: Schema.Types.ObjectId, ref: 'Camera'}]
});

module.exports = mongoose.model('Layout', schema);