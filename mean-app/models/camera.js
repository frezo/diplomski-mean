var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var mongooseUniqueValidator = require('mongoose-unique-validator');

var schema = new Schema({
    name: {type: String, required: true, unique: true},
    location: {type: String, required: true},
    ip_address: {type: String, required: true},
    image_format: {type: String},
    camera_type: {type: String}
});

schema.plugin(mongooseUniqueValidator);

module.exports = mongoose.model('Camera', schema);